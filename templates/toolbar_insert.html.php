<?php

/**
 * This template will be inserted in all HTML responses, right before the </body> tag.
 */

/* @var $debugUrl string */
/* @var $bugiconUrl string */

/**
 * Since the idea is to make this as much standalone as possible, all CSS is injected in style="" attributes,
 * so not a single keyword can interfere with the existing page.
 *
 * To still work with some kind of css, I defined some variables to use in the template.
 */
$cssPosition = 'position: fixed; bottom: 5px; right: 5px;';
$cssContainer = 'border-radius: 10px; border: 1px solid #ccc; background-color: #fff; padding: 8px; color: #000;';
$cssButton = 'float: left; min-width: 45px; border-right: 1px solid #ccc; padding: 0 2px 0 2px; position: relative;';
$cssPopup = $cssContainer . ' position:absolute; bottom: 30px; right: 10px; display: none;';
$cssTableHeader = 'white-space: nowrap; background-color: rgb(209, 236, 241); padding: 8px; border: 1px solid rgb(222, 226, 230); color: rgb(12, 84, 96);';
?>
<div style="<?= htmlspecialchars($cssPosition . ' ' . $cssContainer) ?>" onmouseleave="document.querySelector('[data-debug-toolbar=showhide]').style.display = 'none';">
    <span data-debug-toolbar="showhide" style="display: none;">
        <div style="<?= htmlspecialchars($cssButton) ?>">
            ⚙️<span data-debug-toolbar="memory"></span>
        </div>
        <div style="<?= htmlspecialchars($cssButton) ?>">
            ⏱️<span data-debug-toolbar="speed"></span>
        </div>
        <div style="<?= htmlspecialchars($cssButton) ?>" onmouseenter="document.querySelector('[data-debug-toolbar=errors]').style.display = 'inherit';" onmouseleave="document.querySelector('[data-debug-toolbar=errors]').style.display = 'none';">
            <div data-debug-toolbar="errors" style="<?= htmlspecialchars($cssPopup) ?>"></div>
            ⚠️<span data-debug-toolbar="errorcount"></span>
        </div>
        <div style="<?= htmlspecialchars($cssButton) ?>" onmouseenter="document.querySelector('[data-debug-toolbar=dumps]').style.display = 'inherit';" onmouseleave="document.querySelector('[data-debug-toolbar=dumps]').style.display = 'none';">
            <div data-debug-toolbar="dumps" style="<?= htmlspecialchars($cssPopup) ?>"></div>
            📝 <span data-debug-toolbar="dumpcount"></span>
        </div>
    </span>
    <a href="<?= htmlspecialchars($debugUrl) ?>" target="_blank" onmousemove="document.querySelector('[data-debug-toolbar=showhide]').style.display = '';">
        <!-- Image falls under the Creative Commons license and is taken from https://thenounproject.com/term/debug/83827/ -->
        <img width="32" height="32" data-debug-toolbar="bugicon" src="<?= htmlspecialchars($bugiconUrl) ?>#bug-green">
    </a>
</div>
<script type="text/javascript">
    with(new XMLHttpRequest()) {
        onload = function () {
            let htmlspecialchars = function(txt) {
                let returnValue = new String(txt).replace(/&/g, '&amp;');
                returnValue = returnValue.replace(/</g, '&lt;');
                returnValue = returnValue.replace(/>/g, '&gt;');
                return returnValue;
            }
            let data = JSON.parse(this.responseText);

            // Load basic values
            document.querySelector('[data-debug-toolbar=errorcount]').textContent = data.errors.length;
            document.querySelector('[data-debug-toolbar=dumpcount]').textContent = data.dumps.length;
            document.querySelector('[data-debug-toolbar=speed]').textContent = data.userTimeMs.toFixed(4) + 's';
            document.querySelector('[data-debug-toolbar=memory]').textContent = data.memoryPeakEmalloc;

            // Fold bar open and color bug icon on error or dump
            if (data.errors.length > 0) {
                document.querySelector('[data-debug-toolbar=bugicon]').src = '<?= htmlspecialchars($bugiconUrl) ?>#bug-red';
                document.querySelector('[data-debug-toolbar=showhide]').style.display = '';
            } else if (data.dumps.length > 0) {
                document.querySelector('[data-debug-toolbar=bugicon]').src = '<?= htmlspecialchars($bugiconUrl) ?>#bug-yellow';
                document.querySelector('[data-debug-toolbar=showhide]').style.display = '';
            }

            // Load dumps
            if (data.dumps.length > 0) {
                var html = '<table>';
                for (let iterator = 0; iterator < data.dumps.length; ++iterator) {
                    html += '<tr><th style="<?= htmlspecialchars($cssTableHeader) ?>">Var dump at ' + htmlspecialchars(data.dumps[iterator].file) + ' line ' + htmlspecialchars(data.dumps[iterator].line) + '</th></tr>';
                    html += '<tr><td style="background-color: #fff;"><pre style="overflow: hidden; max-height: 140px;">' + data.dumps[iterator].data + '</pre></td></tr>';
                }
                html += '</table>';
                document.querySelector('[data-debug-toolbar=dumps]').innerHTML = html;
            } else {
                document.querySelector('[data-debug-toolbar=dumps').parentElement.onmouseenter = null;
            }

            // Load errors
            if (data.errors.length > 0) {
                var html = '<table>';
                for (let iterator = 0; iterator < data.errors.length; ++iterator) {
                    html += '<tr><th style="<?= htmlspecialchars($cssTableHeader) ?>">' + data.errors[iterator].errtype + ' at ' + htmlspecialchars(data.errors[iterator].file) + ' line ' + htmlspecialchars(data.errors[iterator].line) + '</th></tr>';
                    html += '<tr><td style="background-color: #fff;"><pre>' + htmlspecialchars(data.errors[iterator].message) + '</pre></td></tr>';
                }
                html += '</table>';
                document.querySelector('[data-debug-toolbar=errors]').innerHTML = html;
            } else {
                document.querySelector('[data-debug-toolbar=errors').parentElement.onmouseenter = null;
            }
        };
        open("GET", "<?= htmlspecialchars($debugUrl) ?>/json", true);
        send();
    }
</script>
