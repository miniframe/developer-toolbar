<?php

/**
 * This template contains the basic debugging output
 */

/* @var $requestMethod string */
/* @var $requestVersion string */
/* @var $requestUri string */
/* @var $requestHeaders array */
/* @var $userTimeMs float */
/* @var $systemTimeMs float */
/* @var $responseHeaders array */
/* @var $responseType string */
/* @var $responseCode int */
/* @var $exitCode int */
/* @var $errors array */
/* @var $memoryPeakReal string */
/* @var $memoryPeakEmalloc string */
/* @var $dumps array */
/* @var $exceptionThrown bool */
/* @var $exceptionType string */
/* @var $exceptionCode int */
/* @var $exceptionMessage string */
/* @var $exceptionFile string */
/* @var $exceptionLine string */
/* @var $exceptionTrace string */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Miniframe Developer Toolbar - <?= htmlspecialchars($requestUri) ?></title>
    <style>
        table.table-hover.table-borderless {
            margin-bottom: 0;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6">
            <table class="table">
                <thead>
                    <tr class="alert alert-info"><th colspan="2">Request</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Request</th>
                        <td><?= htmlspecialchars($requestMethod . ' ' . $requestUri . ' ' . $requestVersion) ?></td>
                    </tr>
                    <tr>
                        <th>Request headers</th>
                        <td>
                            <table class="table table-sm table-hover table-borderless">
                                <?php foreach ($requestHeaders as $key => $value) : ?>
                                    <tr><td><?= htmlspecialchars($key . ': ' . $value) ?></td></tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table">
                <thead>
                    <tr class="alert alert-info"><th colspan="2">Response</th></tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Response</th>
                        <td><?= htmlspecialchars($responseCode) ?></td>
                    </tr>
                    <tr>
                        <th>Response type</th>
                        <td><?= htmlspecialchars($responseType) ?></td>
                    </tr>
                    <tr>
                        <th>Response headers</th>
                        <td>
                            <table class="table table-sm table-hover table-borderless">
                                <?php foreach ($responseHeaders as $responseHeader) : ?>
                                    <tr><td><?= htmlspecialchars($responseHeader['header']) ?></td></tr>
                                <?php endforeach; ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th>User time</th>
                        <td><?= number_format($userTimeMs, 6) ?>s</td>
                    </tr>
                    <tr>
                        <th>System time</th>
                        <td><?= number_format($systemTimeMs, 6) ?>s</td>
                    </tr>
                    <tr>
                        <th>Peak memory (real)</th>
                        <td><?= htmlspecialchars($memoryPeakReal) ?></td>
                    </tr>
                    <tr>
                        <th>Peak memory (emalloc)</th>
                        <td><?= htmlspecialchars($memoryPeakEmalloc) ?></td>
                    </tr>
                </tbody>
            </table>
            <?php if ($exceptionThrown) : ?>
                <table class="table">
                    <thead>
                        <tr class="alert alert-info"><th colspan="2">Exception</th></tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Type</th>
                            <td><?= htmlspecialchars($exceptionType) ?></td>
                        </tr>
                        <tr>
                            <th>Code</th>
                            <td><?= htmlspecialchars($exceptionCode) ?></td>
                        </tr>
                        <tr>
                            <th>Message</th>
                            <td><?= htmlspecialchars($exceptionMessage) ?></td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <td><?= htmlspecialchars($exceptionFile . ' line ' . $exceptionLine) ?></td>
                        </tr>
                        <tr>
                            <th>Backtrace</th>
                            <td>
                                <table class="table table-sm table-hover table-borderless">
                                    <?php foreach (array_reverse(explode(PHP_EOL, $exceptionTrace)) as $line) : ?>
                                        <tr><td><?= htmlspecialchars($line) ?></td></tr>
                                    <?php endforeach; ?>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php endif; /*$exceptionThrown*/ ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php foreach ($errors as $error) : ?>
                <hr>
                <table class="table">
                    <tr>
                        <th class="alert alert-<?= array(E_NOTICE => 'info', E_WARNING => 'warning')[$error['errno']] ?? 'danger' ?>" colspan="2">
                            <?= htmlspecialchars($error['errtype'] . ': ' . $error['message'] . ' in ' . $error['file'] . ' line ' . $error['line']) ?>
                        </th>
                    </tr>
                    <tr><td>Backtrace:</td><td>
                        <table class="table table-sm table-hover table-borderless">
                            <?php foreach ($error['backtrace'] as $backtrace) : ?>
                                <tr>
                                    <td>
                                        <?php if (isset($backtrace['file']) && isset($backtrace['line'])) : ?>
                                            <?= htmlspecialchars($backtrace['file'] . ':' . $backtrace['line']) ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if (isset($backtrace['class']) && isset($backtrace['type'])) : ?>
                                            <?= htmlspecialchars($backtrace['class'] . $backtrace['type'] . $backtrace['function']) ?>()
                                        <?php elseif (isset($backtrace['function'])) : ?>
                                            <?= htmlspecialchars($backtrace['function']) ?>()
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </td></tr>
                </table>
            <?php endforeach; ?>
            <?php foreach ($dumps as $dump) : ?>
                <hr>
                <table class="table">
                    <tr>
                        <th class="alert alert-info">
                            Var dump at <?= htmlspecialchars($dump['file'] . ' line ' . $dump['line']) ?>
                        </th>
                    </tr>
                    <tr><td><pre><?= \Miniframe\Toolbar\Controller\DeveloperToolbar::dumpDataToHtml($dump['data']) ?></pre></td></tr>
                </table>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
