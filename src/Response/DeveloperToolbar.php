<?php

namespace Miniframe\Toolbar\Response;

use Miniframe\Core\Response;
use Miniframe\Response\InternalServerErrorResponse;
use Miniframe\Response\PhpResponse;
use Miniframe\Toolbar\Service\DeveloperToolbar as DeveloperToolbarService;

class DeveloperToolbar extends Response
{
    /**
     * The parent response.
     *
     * @var Response
     */
    private $parentResponse;
    /**
     * URL for this debugging session.
     *
     * @var string
     */
    private $debugUrl;

    /**
     * Response that extends another response and adds the development toolbar to the output HTML
     *
     * @param Response $parentResponse The parent response.
     * @param string   $debugUrl       URL for this debugging session.
     */
    public function __construct(Response $parentResponse, string $debugUrl)
    {
        $this->parentResponse = $parentResponse;
        $this->debugUrl = $debugUrl;
    }

    /**
     * Returns the response text
     *
     * @return string
     */
    public function render(): string
    {
        return $this->addJsonToolbar($this->addHtmlToolbar($this->parentResponse->render()));
    }

    /**
     * Replaces the toolbar in existing json code
     *
     * @param string $jsonData The json code.
     *
     * @return string
     */
    private function addJsonToolbar(string $jsonData): string
    {
        // Validate if we're dealing with json data
        if (
            substr($jsonData, 0, 1) !== '{'
            || substr($jsonData, -1) !== '}'
            || !is_array(json_decode($jsonData, true, 255, JSON_INVALID_UTF8_IGNORE))
        ) {
            return $jsonData;
        }
        $service = DeveloperToolbarService::getInstance();

        // Detect how the json is pretty-printed
        preg_match('/^\{([\r\n]*)([\s]*)/', $jsonData, $matches);
        $eol = $matches[1] ?? '';
        $ws = $matches[2] ?? '';
        $parts = preg_split('/[\s]*\}$/', $jsonData);

        // Start adding a debugging element
        $jsonData = $parts[0] . ',' . $eol;
        $jsonData .= $ws . '"_debugging_' . array_reverse(explode('/', $this->debugUrl))[0] . '": {' . $eol;
        $jsonData .= $ws . $ws . '"debug_url": "' . addslashes($this->debugUrl) . '"';

        // Adding dumps
        if (count($service->getDumps())) {
            $jsonData .= ',' . $eol . $ws . $ws . '"dumps": [';
            foreach ($service->getDumps() as $iterator => $dump) {
                if ($iterator !== 0) {
                    $jsonData .= ',';
                }
                $jsonData .= $eol . $ws . $ws . $ws . '{' . $eol;
                $jsonData .= $ws . $ws . $ws . $ws . '"file": ' . json_encode($dump['file']) . ',' . $eol;
                $jsonData .= $ws . $ws . $ws . $ws . '"line": ' . intval($dump['line']) . ',' . $eol;
                $jsonData .= $ws . $ws . $ws . $ws . '"data": ' . json_encode($dump['data']) . $eol;
                $jsonData .= $ws . $ws . $ws . '}';
            }
            $jsonData .= $eol . $ws . $ws . ']';
        }

        // Adding errors
        if (count($service->getErrors())) {
            $jsonData .= ',' . $eol . $ws . $ws . '"errors": [';
            foreach ($service->getErrors() as $iterator => $error) {
                if ($iterator !== 0) {
                    $jsonData .= ',';
                }
                $jsonData .= $eol . $ws . $ws . $ws . '{' . $eol;
                $jsonData .= $ws . $ws . $ws . $ws . '"errtype": "' . addslashes($error['errtype']) . '",' . $eol;
                $jsonData .= $ws . $ws . $ws . $ws . '"file": "' . addslashes($error['file']) . '",' . $eol;
                $jsonData .= $ws . $ws . $ws . $ws . '"line": ' . intval($error['line']) . ',' . $eol;
                $jsonData .= $ws . $ws . $ws . $ws . '"message": ' . json_encode($error['message']) . $eol;
                $jsonData .= $ws . $ws . $ws . '}';
            }
            $jsonData .= $eol . $ws . $ws . ']';
        }

        // Add backtrace if available
        if ($this->parentResponse instanceof InternalServerErrorResponse || $this->parentResponse->getPrevious()) {
            $exception = $this->parentResponse->getPrevious() ?? $this->parentResponse;
            $jsonData .= ',' . $eol . $ws . $ws . '"exception": ' . json_encode(get_class($exception));
            $jsonData .= ',' . $eol . $ws . $ws . '"message": ' . json_encode($exception->getMessage());
            $jsonData .= ',' . $eol . $ws . $ws . '"code": ' . $exception->getCode();
            $jsonData .= ',' . $eol . $ws . $ws . '"file": ' . json_encode($exception->getFile());
            $jsonData .= ',' . $eol . $ws . $ws . '"line": ' . $exception->getLine();
            $jsonData .= ',' . $eol . $ws . $ws . '"backtrace": ' . json_encode($exception->getTraceAsString());
        }

        // Close debugging element and json
        $jsonData .= $eol . $ws . '}' . $eol . '}';

        return $jsonData;
    }

    /**
     * Replaces the toolbar in existing HTML code
     *
     * @param string $htmlData The HTML code.
     *
     * @return string
     */
    private function addHtmlToolbar(string $htmlData): string
    {
        // Look for HTML data, that contains a body
        if (!preg_match('/<html.*>.*?<\/body>/is', $htmlData)) {
            return $htmlData;
        }

        // Renders the toolbar HTML
        $insertResponse = new PhpResponse(__DIR__ . '/../../templates/toolbar_insert.html.php', [
            'debugUrl' => $this->debugUrl,
            'bugiconUrl' => substr($this->debugUrl, 0, strrpos($this->debugUrl, '/')) . '/bugicon.svg',
        ]);
        $html = $insertResponse->render();

        return preg_replace('/<\/body>/i', $html . '$0', $htmlData, 1);
    }

    /**
     * Returns the current response code (used for HTTP responses)
     *
     * @return integer
     */
    public function getResponseCode(): int
    {
        return $this->parentResponse->getResponseCode();
    }

    /**
     * Returns the current exit code (used for shell error codes)
     *
     * @return integer
     */
    public function getExitCode(): int
    {
        return $this->parentResponse->getExitCode();
    }

    /**
     * Sets a new response code (used for HTTP responses)
     *
     * @param integer $responseCode The new response code.
     *
     * @return void
     */
    public function setResponseCode(int $responseCode): void
    {
        $this->parentResponse->setResponseCode($responseCode);
    }

    /**
     * Adds a response header
     *
     * @param string  $header  The header string.
     * @param boolean $replace When set to false, a header can be sent back multiple times.
     *
     * @return void
     */
    public function addHeader(string $header, bool $replace = true): void
    {
        $this->parentResponse->addHeader($header, $replace);
    }

    /**
     * Sets a new exit code (used for shell error codes)
     *
     * @param integer $exitCode Error code.
     *
     * @return void
     */
    public function setExitCode(int $exitCode): void
    {
        $this->parentResponse->setExitCode($exitCode);
    }

    /**
     * Returns a list of all headers to set (each record has two keys: 'header' and 'replace')
     *
     * @return array[]
     */
    public function getHeaders(): array
    {
        return $this->parentResponse->getHeaders();
    }
}
