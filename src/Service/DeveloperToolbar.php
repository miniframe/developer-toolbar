<?php

namespace Miniframe\Toolbar\Service;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use RuntimeException;

class DeveloperToolbar
{
    /**
     * Path for the log files.
     *
     * @var string
     */
    private $logPath;
    /**
     * Path to the log file for the current request.
     *
     * @var string
     */
    private $logFile;
    /**
     * Hash for the current request.
     *
     * @var string
     */
    private $requestHash;
    /**
     * Data that is logged for the current request.
     *
     * @var array
     */
    private $logData;
    /**
     * Set to true to disable logging for this request
     *
     * @var bool
     */
    private $disabled = false;

    /**
     * Log retention in seconds (14400s = 4h)
     */
    private const LOG_RETENTION = 14400;

    /**
     * Reference to a DeveloperToolbar instance
     *
     * @var DeveloperToolbar|null
     */
    private static $developerToolbar;

    /**
     * Initiates the Developer Toolbar service
     *
     * @param Request $request Reference to the Request object.
     * @param string  $logPath Path in which log files are stored.
     */
    public function __construct(Request $request, string $logPath)
    {
        // Store basic data
        $this->logData['rusage']['start'] = getrusage();
        $this->logData['request'] = $request;
        $this->logData['requestHeaders'] = function_exists('getallheaders') ? getallheaders() : array();
        $this->logData['dumps'] = array();

        // Defines the log file
        $this->requestHash = $this->generateRequestHash();
        $this->logPath = $logPath;
        if (!is_dir($this->logPath)) {
            mkdir($this->logPath, 0777, true);
        }
        $this->logFile = rtrim($logPath, '/') . '/' . $this->requestHash . '.debug';

        // Catches all kind of errors
        $this->logData['errors'] = array();
        set_error_handler(function (int $errno, string $message, string $file, int $line): bool {
            $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            array_shift($backtrace); // Remove the error handler itself
            $table = [
                E_ERROR => 'E_ERROR',
                E_WARNING => 'E_WARNING',
                E_PARSE => 'E_PARSE',
                E_NOTICE => 'E_NOTICE',
                E_CORE_ERROR => 'E_CORE_ERROR',
                E_CORE_WARNING => 'E_CORE_WARNING',
                E_COMPILE_ERROR => 'E_COMPILE_ERROR',
                E_COMPILE_WARNING => 'E_COMPILE_WARNING',
                E_USER_ERROR => 'E_USER_ERROR',
                E_USER_WARNING => 'E_USER_WARNING',
                E_USER_NOTICE => 'E_USER_NOTICE',
                E_STRICT => 'E_STRICT',
                E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
                E_DEPRECATED => 'E_DEPRECATED',
                E_USER_DEPRECATED => 'E_USER_DEPRECATED',
            ];
            $this->logData['errors'][] = array(
                'errno'   => $errno,
                'errtype' => $table[$errno] ?? 'Unknown',
                'message' => $message,
                'file'    => $file,
                'line'    => $line,
                'backtrace' => $backtrace,
            );
            return true; // Error catched/logged, don't show it in the page itself.
        }, E_ALL);

        // Register for static calls (mainly dump())
        self::$developerToolbar = $this;
    }

    /**
     * Returns a reference to a DeveloperToolbar instance
     *
     * @return DeveloperToolbar|null
     */
    public static function getInstance(): ?DeveloperToolbar
    {
        return self::$developerToolbar;
    }

    /**
     * Returns the request hash
     *
     * @return string
     */
    public function getRequestHash(): string
    {
        return $this->requestHash;
    }

    /**
     * Fetched logged data by hash.
     *
     * @param string $hash The hash.
     *
     * @return array|null
     */
    public function getDataByHash(string $hash): ?array
    {
        if (!preg_match('/^[a-zA-Z0-9_\-.]+$/', $hash)) {
            return null;
        }
        $logFile = $this->logPath . '/' . $hash . '.debug';
        if (!file_exists($logFile)) {
            return null;
        }
        return unserialize(file_get_contents($logFile));
    }

    /**
     * Generates a somewhat random/unique string that matches /^[a-zA-Z0-9_\-.]+$/
     *
     * @return string
     */
    private function generateRequestHash(): string
    {
        $binary = '';
        foreach (str_split(date('YmdHis') . substr(explode(' ', microtime())[0], 2), 2) as $char) {
            $binary .= chr($char);
        }
        for ($iterator = 0; $iterator < 4; ++$iterator) {
            $binary .= chr(rand(0x00, 0xff));
        }
        return str_replace(['+', '/', '='], ['_', '-', '.'], base64_encode($binary));
    }

    /**
     * Adds data to the Developer Toolbar log
     *
     * @param string $key  Key of the data.
     * @param mixed  $data Serializable log data.
     *
     * @return void
     */
    public function addData(string $key, $data): void
    {
        $this->logData[$key] = $data;
    }

    /**
     * Disables or enables logging.
     *
     * @param boolean $isDisabled The disabled state.
     *
     * @return void
     */
    public function setDisabled(bool $isDisabled): void
    {
        $this->disabled = $isDisabled;
    }

    /**
     * Return the disabled state.
     *
     * @return boolean
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * Dumps a variable
     *
     * @param mixed $variable The variable to dump.
     *
     * @return void
     */
    public function dump($variable): void
    {
        // Where is the dump() called?
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        $backtraceIndex = 0;
        if (
            isset($backtrace[$backtraceIndex]['file'])
            && realpath($backtrace[$backtraceIndex]['file']) == realpath(__DIR__ . '/../functions.php')
        ) {
            $backtraceIndex = 1;
        }
        $data = [
            'file' => $backtrace[$backtraceIndex]['file'] ?? null,
            'line' => $backtrace[$backtraceIndex]['line'] ?? null,
            'data' => $variable,
        ];

        $this->logData['dumps'][] = $data;
    }

    /**
     * Returns all dumps
     *
     * @return array
     */
    public function getDumps(): array
    {
        return $this->logData['dumps'] ?? [];
    }

    /**
     * Returns all errors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->logData['errors'] ?? [];
    }

    /**
     * Store the log data and cleans up old logs
     */
    public function __destruct()
    {
        if ($this->disabled) {
            return;
        }

        $this->logData['rusage']['end'] = getrusage();
        $this->logData['memoryPeak']['real'] = memory_get_peak_usage(true);
        $this->logData['memoryPeak']['emalloc'] = memory_get_peak_usage();
        file_put_contents($this->logFile, serialize($this->logData));
        if (!file_exists($this->logFile)) {
            throw new \RuntimeException('Can\'t write to ' . $this->logFile);
        }

        // Clean up old files
        $files = glob($this->logPath . '/*.debug');
        foreach ($files as $file) {
            if (filemtime($file) < time() - self::LOG_RETENTION) {
                unlink($file);
            }
        }
    }
}
