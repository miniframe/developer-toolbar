<?php

namespace Miniframe\Toolbar\Controller;

use Miniframe\Core\AbstractController;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\JsonResponse;
use Miniframe\Response\PhpResponse;

class DeveloperToolbar extends AbstractController
{
    /**
     * All logged data from the request
     *
     * @var array
     */
    private $requestData;

    /**
     * Initiates the Developer Toolbar Controller
     *
     * @param Request $request     Reference to the Request object.
     * @param Config  $config      Reference to the Config object.
     * @param array   $requestData Data that's linked to the debugging request.
     */
    public function __construct(Request $request, Config $config, array $requestData = array())
    {
        $this->requestData = $requestData;
        parent::__construct($request, $config);
    }

    /**
     * Interprets all data and returns it to an associative array
     *
     * @return array
     */
    private function interpretData(): array
    {
        $originalRequest = $this->requestData['request'];
        /* @var $originalRequest Request */

        $data = array();

        $data['requestMethod'] = $originalRequest->getServer('REQUEST_METHOD');
        $data['requestVersion'] = $originalRequest->getServer('SERVER_PROTOCOL');
        $data['requestUri'] = $originalRequest->getServer('REQUEST_URI');
        $data['requestHeaders'] = $this->requestData['requestHeaders'];
        $data['userTimeMs'] = $this->calculateRusageTime($this->requestData['rusage'], 'utime');
        $data['systemTimeMs'] = $this->calculateRusageTime($this->requestData['rusage'], 'stime');

        $data['responseHeaders'] = $this->requestData['responseHeaders'];
        $data['responseType'] = $this->requestData['responseType'];
        $data['responseCode'] = $this->requestData['responseCode'];
        $data['exitCode'] = $this->requestData['exitCode'];

        $data['errors'] = $this->requestData['errors'];
        $data['memoryPeakReal'] = $this->formatBytes($this->requestData['memoryPeak']['real']);
        $data['memoryPeakEmalloc'] = $this->formatBytes($this->requestData['memoryPeak']['emalloc']);

        $data['exceptionThrown'] = $this->requestData['exceptionThrown'];
        $data['exceptionType'] = $this->requestData['exceptionType'];
        $data['exceptionCode'] = $this->requestData['exceptionCode'];
        $data['exceptionMessage'] = $this->requestData['exceptionMessage'];
        $data['exceptionFile'] = $this->requestData['exceptionFile'];
        $data['exceptionLine'] = $this->requestData['exceptionLine'];
        $data['exceptionTrace'] = $this->requestData['exceptionTrace'];

        $data['dumps'] = $this->requestData['dumps'];

        return $data;
    }

    /**
     * /_DEBUG/[hash] page; shows the debugging output in HTML
     *
     * @return Response
     */
    public function view(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/toolbar_view.html.php', $this->interpretData());
    }

    /**
     * /_DEBUG/[hash]/json page; outputs the debugging data in json format.
     *
     * @return Response
     */
    public function json(): Response
    {
        $data = $this->interpretData();
        // Convert dumps
        foreach ($data['dumps'] as &$dump) {
            $dump['data'] = static::dumpDataToHtml($dump['data']);
        }
        return new JsonResponse($data);
    }

    /**
     * Converts dump data to nicely formatted HTML
     *
     * @param mixed $dumpData The dump data.
     *
     * @return string
     */
    public static function dumpDataToHtml($dumpData): string
    {
        $varExport = var_export($dumpData, true);
        $highlighted = highlight_string('<?php ' . $varExport, true);

        $phpStripped = preg_replace('/<span[^>]+>&lt;\?php&nbsp;<\/span>/', '', $highlighted);
        $firstNewlineStripped = preg_replace("/\n/", '', $phpStripped, 1);
        $lastNewlineStripped = str_replace("\n</code>", '', $firstNewlineStripped);

        return $lastNewlineStripped;
    }

    /**
     * Returns the bugicon.svg asset.
     *
     * @return Response
     */
    public function bugicon(): Response
    {
        $return = new PhpResponse(__DIR__ . '/../../templates/bugicon.svg.php');
        $return->addHeader('Content-Type: image/svg+xml');
        return $return;
    }

    /**
     * Calculates the difference between start and end.
     *
     * @param array  $rusageData Rusage data array.
     * @param string $key        Key for which we want the difference.
     *
     * @return float
     */
    private function calculateRusageTime(array $rusageData, string $key): float
    {
        $start = $rusageData['start'];
        $end = $rusageData['end'];

        $startTimestamp = $start['ru_' . $key . '.tv_sec'] . '.' . $start['ru_' . $key . '.tv_usec'];
        $endTimestamp = $end['ru_' . $key . '.tv_sec'] . '.' . $end['ru_' . $key . '.tv_usec'];

        return (float)$endTimestamp - (float)$startTimestamp;
    }

    /**
     * Formats an amount of bytes in a human-readable format
     *
     * @param integer $bytes The amount of bytes.
     *
     * @return string
     */
    private function formatBytes(int $bytes): string
    {
        if ($bytes > 1073741824) {
            return number_format($bytes / 1073741824, 2) . 'GB';
        }
        if ($bytes > 1048576) {
            return number_format($bytes / 1048576, 2) . 'MB';
        }
        if ($bytes > 1024) {
            return number_format($bytes / 1024, 2) . 'kB';
        }
        return $bytes . ' bytes';
    }
}
