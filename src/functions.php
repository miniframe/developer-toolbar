<?php

/**
 * This file is always included and contains the dump() wrapper
 */

if (!function_exists('dump')) {
    /**
     * Dumps a variable
     *
     * @param mixed $variable The variable to dump.
     *
     * @return void
     */
    function dump($variable): void
    {
        $instance = \Miniframe\Toolbar\Service\DeveloperToolbar::getInstance();
        if ($instance) {
            $instance->dump($variable);
        } else {
            var_dump($variable);
        }
    }
}
