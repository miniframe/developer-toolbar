<?php

namespace Miniframe\Toolbar\Middleware;

use Miniframe\Toolbar\Service\DeveloperToolbar as DeveloperToolbarService;
use Miniframe\Toolbar\Controller\DeveloperToolbar as DeveloperToolbarController;
use Miniframe\Toolbar\Response\DeveloperToolbar as DeveloperToolbarResponse;
use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Core\Response;

class DeveloperToolbar extends AbstractMiddleware
{
    /**
     * Reference to the Developer Toolbar service
     *
     * @var DeveloperToolbarService
     */
    protected $developerToolbar;

    /**
     * Prefix URL for the developer toolbar
     *
     * @var string
     */
    protected $debugPrefixUrl;

    /**
     * Initiates the Developer Toolbar middleware; binding to the framework and starting the service.
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        if (!$config->has('developer-toolbar', 'log_path')) {
            throw new \RuntimeException('No log path configured for the developer toolbar');
        }

        // Creates and registers the Developer Toolbar service
        $this->developerToolbar = new DeveloperToolbarService(
            $request,
            $config->getPath('developer-toolbar', 'log_path')
        );
        Registry::register(DeveloperToolbarService::class, $this->developerToolbar);

        // Generates the URL
        $this->debugPrefixUrl = $config->get('framework', 'base_href') . '_DEBUG';

        parent::__construct($request, $config);
    }

    /**
     * Adds the Developer Toolbar routes
     *
     * @return array
     */
    public function getRouters(): array
    {
        return [function (): ?callable {
            $comparePath = explode('/', trim($this->debugPrefixUrl, '/'));
            foreach ($comparePath as $index => $path) {
                if ($this->request->getPath($index) != $path) {
                    return null;
                }
            }

            // Is the hash valid?
            $hash = $this->request->getPath(count($comparePath));
            if ($hash === 'bugicon.svg') {
                $this->developerToolbar->setDisabled(true);
                return [new DeveloperToolbarController($this->request, $this->config), 'bugicon'];
            }
            if (!preg_match('/^[a-zA-Z0-9_\-.]+$/', $hash)) {
                return null;
            }
            $requestData = $this->developerToolbar->getDataByHash($hash);
            if ($requestData === null) {
                return null;
            }

            // Is the subcommand valid?
            $subCommand = $this->request->getPath(count($comparePath) + 1);
            if ($subCommand && !method_exists(DeveloperToolbarController::class, $subCommand)) {
                return null;
            }

            $this->developerToolbar->setDisabled(true);
            return [new DeveloperToolbarController($this->request, $this->config, $requestData), $subCommand ?? 'view'];
        }];
    }

    /**
     * Modifies output to append the Developer Toolbar URL in headers and HTML pages
     *
     * @todo Modify HTML pages
     *
     * @return array
     */
    public function getPostProcessors(): array
    {
        return [function (Response $response, bool $thrown): Response {
            // When disabled, return actual response
            if ($this->developerToolbar->isDisabled()) {
                return $response;
            }

            // Make the URL absolute
            $hostPrefix = 'http';
            if ($this->request->isHttpsRequest()) {
                $hostPrefix .= 's';
            }
            $hostPrefix .= '://' . $this->request->getServer('HTTP_HOST');

            // Combine all URL parts
            $debugUrl = $hostPrefix . $this->debugPrefixUrl . '/' . $this->developerToolbar->getRequestHash();

            $response->addHeader('X-Debug-URL: ' . $debugUrl);
            $this->developerToolbar->addData('responseHeaders', $response->getHeaders());
            $this->developerToolbar->addData('responseCode', $response->getResponseCode());
            $this->developerToolbar->addData('responseType', get_class($response));
            $this->developerToolbar->addData('exitCode', $response->getExitCode());

            // Add exception data
            $exception = $response->getPrevious() ?? $response;
            $this->developerToolbar->addData('exceptionThrown', $thrown);
            $this->developerToolbar->addData('exceptionType', get_class($exception));
            $this->developerToolbar->addData('exceptionCode', $exception->getCode());
            $this->developerToolbar->addData('exceptionMessage', $exception->getMessage());
            $this->developerToolbar->addData('exceptionFile', $exception->getFile());
            $this->developerToolbar->addData('exceptionLine', $exception->getLine());
            $this->developerToolbar->addData('exceptionTrace', $exception->getTraceAsString());

            return new DeveloperToolbarResponse($response, $debugUrl);
        }];
    }
}
