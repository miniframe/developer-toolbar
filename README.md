# Miniframe Developer Toolbar

This library adds a basic developer toolbar to the [Miniframe PHP Framework](https://miniframe.dev/).

[![build](https://miniframe.dev/build/badge/developer-toolbar)](https://bitbucket.org/miniframe/developer-toolbar/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/developer-toolbar)](https://miniframe.dev/codecoverage/developer-toolbar)

## How to install

1. In your existing project, type: `composer require --dev miniframe/developer-toolbar`
2. Add the directive below to a `/config/development.ini`
3. Add the development.ini to your `.gitignore` (you won't want to have this running on production!)

*Example development.ini directives*
```ini
[framework]
middleware[] = Miniframe\Toolbar\Middleware\DeveloperToolbar

[developer-toolbar]
log_path = cache/debug
```

Add the Middleware as high as possible in the Middleware list,
so it's loaded before other middlewares get the chance of throwing errors.

## How to use

On each HTML page, a bug icon is added. If you click on it, a new tab will open with debugging information.  
When debugging non-HTML pages, look at the headers (Most browsers: F12 -> Network tab) and locate the `X-Debug-URL` header.
If you open that URL, you will also get the debugging page.

In code, you can add var dumps to the debugging page by using:
```php
dump(['foo', 'bar']);
```

It's also possible to add custom errors to the debugging page by using:
```php
// notice:
trigger_error('foo bar');

// warning:
trigger_error('foo bar', E_USER_WARNING);

// or a deprecation notice:
trigger_error("Since Core v1.0.0: Test deprecation", E_USER_DEPRECATED);
```

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/developer-toolbar/addon/pipelines/home) to automatically test this project.
