<?php

namespace Miniframe\Toolbar\Response;

use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\InternalServerErrorResponse;
use Miniframe\Response\JsonResponse;
use Miniframe\Toolbar\AbstractDeveloperToolbarTest;
use Miniframe\Toolbar\Service\DeveloperToolbar as DeveloperToolbarService;

class DeveloperToolbarTest extends AbstractDeveloperToolbarTest
{
    /**
     * Tests the render() method
     *
     * @return void
     */
    public function testRender(): void
    {
        $preResponse = new Response('foo bar');
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
        $this->assertEquals($preResponse->render(), $postResponse->render());
    }

    /**
     * Tests the addHtmlToolbar() method w/o HTML-code as input
     *
     * @return void
     */
    public function testAddHtmlToolbarFail(): void
    {
        $preResponse = new Response('<xml><not-html>foo bar</not-html></xml>');
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
        $this->assertEquals($preResponse->render(), $postResponse->render());
    }

    /**
     * Tests the addHtmlToolbar() method with HTML-code as input
     *
     * @return void
     */
    public function testAddHtmlToolbarSuccess(): void
    {
        $preResponse = new Response('<html><body>foo bar</body></html>');
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
        $this->assertNotEquals($preResponse->render(), $postResponse->render());
    }

    /**
     * Tests the addJsonToolbar() method with a Json object as input
     *
     * @return void
     */
    public function testAddJsonToolbarSuccess(): void
    {
        new DeveloperToolbarService(new Request(['REQUEST_URI' => '/']), static::$logPath);

        $preResponse = new JsonResponse(['Foo' => 'Bar']);
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
        $this->assertNotEquals($preResponse->render(), $postResponse->render());
    }

    /**
     * Tests the addJsonToolbar() method with a Json object as input and dumps included
     *
     * @return void
     */
    public function testAddJsonToolbarWithDumps(): void
    {
        new DeveloperToolbarService(new Request(['REQUEST_URI' => '/']), static::$logPath);

        dump('foobar');
        dump('baz');
        $line = __LINE__ - 2;
        $file = __FILE__;

        $preResponse = new JsonResponse(['Foo' => 'Bar']);
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
        $this->assertJson($postResponse->render());

        $decoded = json_decode($postResponse->render(), true);
        $this->assertEquals($file, $decoded['_debugging_mysecrethash']['dumps'][0]['file']);
        $this->assertEquals($line, $decoded['_debugging_mysecrethash']['dumps'][0]['line']);
        $this->assertEquals('foobar', $decoded['_debugging_mysecrethash']['dumps'][0]['data']);

        $this->assertEquals('baz', $decoded['_debugging_mysecrethash']['dumps'][1]['data']);
    }

    /**
     * Tests the addJsonToolbar() method with a Json object as input with errors included
     *
     * @return void
     */
    public function testAddJsonToolbarWithErrors(): void
    {
        new DeveloperToolbarService(new Request(['REQUEST_URI' => '/']), static::$logPath);

        trigger_error('foobar', E_USER_ERROR);
        trigger_error('baz', E_USER_ERROR);
        $line = __LINE__ - 2;
        $file = __FILE__;

        $preResponse = new JsonResponse(['Foo' => 'Bar']);
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
        $this->assertJson($postResponse->render());

        $decoded = json_decode($postResponse->render(), true);
        $this->assertEquals('E_USER_ERROR', $decoded['_debugging_mysecrethash']['errors'][0]['errtype']);
        $this->assertEquals($file, $decoded['_debugging_mysecrethash']['errors'][0]['file']);
        $this->assertEquals($line, $decoded['_debugging_mysecrethash']['errors'][0]['line']);
        $this->assertEquals('foobar', $decoded['_debugging_mysecrethash']['errors'][0]['message']);
        $this->assertEquals('baz', $decoded['_debugging_mysecrethash']['errors'][1]['message']);
    }

    /**
     * Tests the addJsonToolbar() method with a Json object as input with exceptions included
     *
     * @return void
     */
    public function testAddJsonToolbarWithExceptions(): void
    {
        new DeveloperToolbarService(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $_SERVER['HTTP_ACCEPT'] = 'application/json';

        $originalException = new \RuntimeException('Foobar', 1337);
        $line = __LINE__ - 1;
        $file = __FILE__;
        $preResponse = new InternalServerErrorResponse('Foo bar', 1338, $originalException);
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
        $result = $postResponse->render();
        unset($_SERVER['HTTP_ACCEPT']);

        $this->assertJson($result);
        $decoded = json_decode($result, true);
        $this->assertEquals(\RuntimeException::class, $decoded['_debugging_mysecrethash']['exception']);
        $this->assertEquals('Foobar', $decoded['_debugging_mysecrethash']['message']);
        $this->assertEquals(1337, $decoded['_debugging_mysecrethash']['code']);
        $this->assertEquals($file, $decoded['_debugging_mysecrethash']['file']);
        $this->assertEquals($line, $decoded['_debugging_mysecrethash']['line']);
        $this->assertIsString($decoded['_debugging_mysecrethash']['backtrace']);
    }

    /**
     * Tests the getResponseCode() output
     *
     * @return void
     */
    public function testGetResponseCode(): void
    {
        foreach ([200, 301, 404] as $responseCode) {
            $preResponse = new Response('foo bar');
            $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
            $preResponse->setResponseCode($responseCode);
            $this->assertEquals($responseCode, $postResponse->getResponseCode());
        }
    }

    /**
     * Tests the getExitCode() output
     *
     * @return void
     */
    public function testGetExitCode(): void
    {
        foreach ([0, 1, 2] as $exitCode) {
            $preResponse = new Response('foo bar', $exitCode);
            $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
            $this->assertEquals($exitCode, $postResponse->getExitCode());
        }
    }

    /**
     * Tests the setResponseCode() output
     *
     * @return void
     */
    public function testSetResponseCode(): void
    {
        foreach ([200, 301, 404] as $responseCode) {
            $preResponse = new Response('foo bar');
            $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
            $postResponse->setResponseCode($responseCode);
            $this->assertEquals($responseCode, $preResponse->getResponseCode());
        }
    }

    /**
     * Tests the setExitCode() output
     *
     * @return void
     */
    public function testSetExitCode(): void
    {
        foreach ([0, 1, 2] as $exitCode) {
            $preResponse = new Response('foo bar');
            $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');
            $postResponse->setExitCode($exitCode);
            $this->assertEquals($exitCode, $preResponse->getExitCode());
        }
    }

    /**
     * Tests the addHeader() and getHeaders() methods.
     *
     * @return void
     */
    public function testHeaderMethods(): void
    {
        $preResponse = new Response('foo bar');
        $postResponse = new DeveloperToolbar($preResponse, '/_DEBUG/mysecrethash');

        // Set on pre, get on post
        $preResponse->addHeader('Content-type: image/png');
        $this->assertArrayHasHeader('content-type', 'image/png', $postResponse->getHeaders());

        // Set on post, get on pre
        $postResponse->addHeader('Content-length: 128');
        $this->assertArrayHasHeader('content-length', '128', $preResponse->getHeaders());

        // Set on pre, get on pre
        $preResponse->addHeader('Content-disposition: inline');
        $this->assertArrayHasHeader('content-disposition', 'inline', $preResponse->getHeaders());

        // Set on post, get on post
        $postResponse->addHeader('Content-Encoding: binary');
        $this->assertArrayHasHeader('content-encoding', 'binary', $postResponse->getHeaders());
    }
}
