<?php

namespace Miniframe\Toolbar;

use Miniframe\Core\Config;
use Miniframe\Toolbar\Service\DeveloperToolbar as DeveloperToolbarService;
use PHPUnit\Framework\TestCase;

abstract class AbstractDeveloperToolbarTest extends TestCase
{
    /**
     * This method is called before the first test of this test class is run.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        // Creates temp folder and makes it working
        static::$logPath = sys_get_temp_dir() . '/DeveloperToolbarTest';
        mkdir(static::$logPath);

        // Creates a fake config
        static::$config = Config::__set_state([
            'data' => [
                'framework' => ['base_href' => '/'],
                'developer-toolbar' => ['log_path' => static::$logPath],
            ],
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
        ]);
    }

    /**
     * Temporary path for log files
     *
     * @var string
     */
    protected static $logPath;

    /**
     * Dummy config object
     *
     * @var Config
     */
    protected static $config;

    /**
     * This method is called after the last test of this test class is run.
     *
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        // Make sure any left-overs are removed
        $files = glob(static::$logPath . '/*.debug');
        if (is_array($files)) {
            foreach ($files as $file) {
                unlink($file);
            }
        }
        rmdir(static::$logPath);
    }

    /**
     * After each test, destroy left-overs of the DeveloperToolbar service, when needed.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        // Dirty way to clean up the service
        $service = DeveloperToolbarService::getInstance();
        if ($service !== null) {
            // Destroy static reference
            $rp = new \ReflectionProperty(DeveloperToolbarService::class, 'developerToolbar');
            $rp->setAccessible(true);
            $rp->setValue(null);
        }
    }

    /**
     * Asserts if an array of headers contains a specific header key
     *
     * @param string      $expectedHeader The header key.
     * @param string|null $expectedValue  The expected value.
     * @param array       $headers        The list of headers.
     *
     * @return void
     */
    public function assertArrayHasHeader(string $expectedHeader, ?string $expectedValue, array $headers): void
    {
        $this->assertIsArray($headers);
        $found = false;
        foreach ($headers as $headerArray) {
            $this->assertArrayHasKey('header', $headerArray);
            $this->assertArrayHasKey('replace', $headerArray);
            $this->assertIsString($headerArray['header']);
            $this->assertIsBool($headerArray['replace']);
            $header = $headerArray['header'];
            if (
                strtolower(substr($header, 0, strlen($expectedHeader) + 1)) == strtolower($expectedHeader . ':')
                && ($expectedValue === null || trim(substr($header, strlen($expectedHeader) + 1)) == $expectedValue)
            ) {
                $found = true;
            }
        }
        $this->assertTrue($found);
    }
}
