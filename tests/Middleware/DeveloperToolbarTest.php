<?php

namespace Miniframe\Toolbar\Middleware;

use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Toolbar\AbstractDeveloperToolbarTest;
use Miniframe\Toolbar\Service\DeveloperToolbar as DeveloperToolbarService;
use Miniframe\Toolbar\Controller\DeveloperToolbar as DeveloperToolbarController;
use Miniframe\Toolbar\Response\DeveloperToolbar as DeveloperToolbarResponse;

class DeveloperToolbarTest extends AbstractDeveloperToolbarTest
{
    /**
     * This method is called before the first test of this test class is run.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        // Fakes a hash
        file_put_contents(static::$logPath . '/validhashandgo.debug', serialize([]));
    }

    /**
     * Tests with an invalid/missing log path in the config
     *
     * @return void
     */
    public function testInvalidLogPath(): void
    {
        $request = new Request(['REQUEST_URI' => '/']);
        $config = Config::__set_state([
            'data' => [],
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
        ]);

        $this->expectException(\RuntimeException::class);
        new DeveloperToolbar($request, $config); // not static::$config, this config is invalid.
    }

    /**
     * Dataprovider for testRouter
     *
     * @return array[]
     */
    public function getRouterDataProvider(): array
    {
        return array(
            // $requestUri                   $controller                        $method
            ['/',                            null,                              null],
            ['/_DEBUG/bugicon.svg',          DeveloperToolbarController::class, 'bugicon'],
            ['/_DEBUG/!nvalidHa$h',          null,                              null],
            ['/_DEBUG/validhashbutno',       null,                              null],
            ['/_DEBUG/validhashandgo',       DeveloperToolbarController::class, 'view'],
            ['/_DEBUG/validhashandgo/json',  DeveloperToolbarController::class, 'json'],
            ['/_DEBUG/validhashandgo/butno', null,                              null],
        );
    }

    /**
     * Tests a specific route with the router
     *
     * @param string      $requestUri The requested URL.
     * @param string|null $controller Name of the expected controller (or null when not found).
     * @param string|null $method     Name of the expected method (or null when not found).
     *
     * @dataProvider getRouterDataProvider
     * @return       void
     */
    public function testGetRouters(string $requestUri, ?string $controller, ?string $method): void
    {
        // Start the middleware
        $middleware = new DeveloperToolbar(new Request(['REQUEST_URI' => $requestUri]), static::$config);

        // Do we have a registered service with one router?
        $this->assertTrue(Registry::has(DeveloperToolbarService::class));
        $this->assertIsArray($middleware->getRouters());
        $this->assertCount(1, $middleware->getRouters());

        // Is the router callable?
        $router = $middleware->getRouters()[0];
        $this->assertIsCallable($router);
        $result = $router();
        if ($controller === null) {
            $this->assertNull($result);
        } else {
            $this->assertIsCallable($result);
            $this->assertInstanceOf(DeveloperToolbarController::class, $result[0]);
            $this->assertEquals($method, $result[1]);
        }
    }

    /**
     * Tests getPostProcessors with both disabled states
     *
     * @return array[]
     */
    public function getPostProcessorsDataProvider(): array
    {
        return array(
            ['disabledState' => false],
            ['disabledState' => true],
        );
    }

    /**
     * Test the getPostProcessors in two stages
     *
     * @param boolean $disabledState The disabled state.
     *
     * @return       void
     * @dataProvider getPostProcessorsDataProvider
     */
    public function testGetPostProcessors(bool $disabledState): void
    {
        // Start the middleware and sets the disabled state
        $middleware = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$config);
        Registry::get(DeveloperToolbarService::class)->setDisabled($disabledState);

        // Do we have a registered service with one post processor?
        $this->assertTrue(Registry::has(DeveloperToolbarService::class));
        $this->assertIsArray($middleware->getPostProcessors());
        $this->assertCount(1, $middleware->getPostProcessors());

        // Is the post processor callable?
        $postProcessor = $middleware->getPostProcessors()[0];
        $this->assertIsCallable($postProcessor);

        // Create a basic response and put it through the postProcessor
        $preResponse = new Response('Foo bar');
        $postResponse = $postProcessor($preResponse, false);

        // Assert it's values
        if ($disabledState) {
            $this->assertEquals($preResponse, $postResponse);
        } else {
            $this->assertInstanceOf(DeveloperToolbarResponse::class, $postResponse);
            $this->assertArrayHasHeader('X-Debug-URL', null, $postResponse->getHeaders());
        }
    }

    /**
     * Test the getPostProcessors with https enabled
     *
     * @return       void
     * @dataProvider getPostProcessorsDataProvider
     */
    public function testGetPostProcessorsWithHttps(): void
    {
        // Start the middleware and sets the disabled state
        $middleware = new DeveloperToolbar(new Request([
            'HTTPS' => 'on',
            'REQUEST_URI' => '/',
            'HTTP_HOST' => 'foobar',
        ]), static::$config);

        $postProcessor = $middleware->getPostProcessors()[0];
        $preResponse = new Response('Foo bar');
        $postResponse = $postProcessor($preResponse, false);
        $this->assertArrayHasHeader('X-Debug-URL', null, $postResponse->getHeaders());
        foreach ($postResponse->getHeaders() as $header) {
            if (strpos($header['header'], 'X-Debug-URL') === false) {
                continue;
            }
            $this->assertStringContainsString('https://foobar/', $header['header']);
        }
    }
}
