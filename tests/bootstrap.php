<?php

namespace autoload {
    if (!isset($GLOBALS['skipAutoload']) || $GLOBALS['skipAutoload'] !== true) {
        require_once __DIR__ . '/../vendor/autoload.php';
    }
    require_once __DIR__ . '/AbstractDeveloperToolbarTest.php';
}
