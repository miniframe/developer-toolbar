<?php

namespace Miniframe\Toolbar\Service;

use Garrcomm\PHPUnitHelpers\FunctionMock;
use Miniframe\Core\Request;
use Miniframe\Toolbar\AbstractDeveloperToolbarTest;

class DeveloperToolbarTest extends AbstractDeveloperToolbarTest
{
    /**
     * This method is called after each test.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * Test getInstance where the value should be null.
     *
     * @return void
     */
    public function testGetInstanceIsNull(): void
    {
        $this->assertNull(DeveloperToolbar::getInstance());
    }

    /**
     * Test getInstance where the value is set.
     *
     * @return void
     */
    public function testGetInstance(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $this->assertEquals($service, DeveloperToolbar::getInstance());
    }

    /**
     * Tests creating the log folder.
     *
     * @return void
     */
    public function testCreateFolder(): void
    {
        $logPath = static::$logPath . 'nonExistent';
        $this->assertFalse(is_dir($logPath));
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), $logPath);
        $this->assertTrue(is_dir($logPath));
    }

    /**
     * Tests if the constructor and destructor add all correct data to the log array.
     *
     * @return void
     */
    public function testLogContents(): void
    {
        // Build new service, calling the constructor
        $request = new Request(['REQUEST_URI' => '/']);
        $service = new DeveloperToolbar($request, static::$logPath);

        // Calling destructor so we can validate all data
        $service->__destruct();
        $data = $service->getDataByHash($service->getRequestHash());

        FunctionMock::mock('Miniframe\\Toolbar\\Service', 'getallheaders', function () {
            return array();
        });

        // Set in constructor
        $this->assertIsNumeric($data['rusage']['start']['ru_utime.tv_sec']);
        $this->assertIsNumeric($data['rusage']['start']['ru_utime.tv_usec']);
        $this->assertIsNumeric($data['rusage']['start']['ru_stime.tv_sec']);
        $this->assertIsNumeric($data['rusage']['start']['ru_stime.tv_usec']);
        $this->assertEquals(getallheaders(), $data['requestHeaders']);
        $this->assertIsArray($data['dumps']);
        $this->assertIsArray($data['errors']);

        // Set in destructor
        $this->assertIsNumeric($data['rusage']['end']['ru_utime.tv_sec']);
        $this->assertIsNumeric($data['rusage']['end']['ru_utime.tv_usec']);
        $this->assertIsNumeric($data['rusage']['end']['ru_stime.tv_sec']);
        $this->assertIsNumeric($data['rusage']['end']['ru_stime.tv_usec']);
        $this->assertIsNumeric($data['memoryPeak']['real']);
        $this->assertIsNumeric($data['memoryPeak']['emalloc']);
    }

    /**
     * Tests the error handler that's defined in the constructor.
     *
     * @return void
     */
    public function testErrorHandler(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        user_error('foo bar', E_USER_NOTICE);
        $line = __LINE__ - 1; // So we can later verify if we got the correct error line

        // Test getErrors()
        $this->assertCount(1, $service->getErrors());

        // Force service to write log, and read it back
        $service->__destruct();
        $data = $service->getDataByHash($service->getRequestHash());

        // Does the data contain the user error?
        $this->assertIsArray($data['errors']);
        $this->assertCount(1, $data['errors']);
        $this->assertEquals(E_USER_NOTICE, $data['errors'][0]['errno']);
        $this->assertEquals('E_USER_NOTICE', $data['errors'][0]['errtype']);
        $this->assertEquals('foo bar', $data['errors'][0]['message']);
        $this->assertEquals(__FILE__, $data['errors'][0]['file']);
        $this->assertEquals($line, $data['errors'][0]['line']);
        $this->assertIsArray($data['errors'][0]['backtrace']);
    }

    /**
     * Tests the getRequestHash() method.
     *
     * @return void
     */
    public function testGetRequestHash(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $this->assertMatchesRegularExpression('/^[a-zA-Z0-9_\-.]+$/', $service->getRequestHash());
    }

    /**
     * Test getDataByHash with an invalid hash
     *
     * @return void
     */
    public function testGetDataByInvalidHash(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $this->assertNull($service->getDataByHash('Th!$isinval!d'));
    }

    /**
     * Test getDataByHash with a non-existent hash
     *
     * @return void
     */
    public function testGetDataByMissingHash(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $this->assertNull($service->getDataByHash('ThisIsValidButNonExistent'));
    }

    /**
     * Tests addData
     *
     * @return void
     */
    public function testAddData(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $service->addData('foo', 'bar');

        // Force service to write log, and read it back
        $service->__destruct();
        $data = $service->getDataByHash($service->getRequestHash());

        $this->assertArrayHasKey('foo', $data);
        $this->assertEquals('bar', $data['foo']);
    }

    /**
     * Tests what happens when setDisabled(true) is called.
     *
     * @return void
     */
    public function testSetDisabledTrue(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $service->setDisabled(true);
        $this->assertEquals(true, $service->isDisabled());

        // Force service to write log, and read it back
        $service->__destruct();
        $data = $service->getDataByHash($service->getRequestHash());

        $this->assertNull($data);
    }

    /**
     * Tests what happens when setDisabled(false) is called.
     *
     * @return void
     */
    public function testSetDisabledFalse(): void
    {
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $service->setDisabled(false);
        $this->assertEquals(false, $service->isDisabled());

        // Force service to write log, and read it back
        $service->__destruct();
        $data = $service->getDataByHash($service->getRequestHash());

        $this->assertIsArray($data);
    }

    /**
     * Tests the dump() method
     *
     * @return void
     */
    public function testDump(): void
    {
        $request = new Request(['REQUEST_URI' => '/']);
        $service = new DeveloperToolbar($request, static::$logPath);
        ob_start();
        $service->dump(['foo', 'bar', $line = __LINE__]);

        // Does getDump() work?
        $this->assertCount(1, $service->getDumps());

        // Output buffer should be empty now
        $data = ob_get_clean();
        $this->assertEquals('', $data);

        // Is the dump in the log?
        $service->__destruct(); // Force a save action
        $log = $service->getDataByHash($service->getRequestHash());
        $this->assertIsArray($log);
        $this->assertArrayHasKey('dumps', $log);
        $this->assertIsArray($log['dumps']);
        $this->assertCount(1, $log['dumps']);

        // Is the dump valid?
        $dump = $log['dumps'][0];
        $this->assertArrayHasKey('file', $dump);
        $this->assertEquals(__FILE__, $dump['file']);
        $this->assertArrayHasKey('line', $dump);
        $this->assertEquals($line, $dump['line']);
        $this->assertArrayHasKey('data', $dump);
        $this->assertEquals(['foo', 'bar', $line], $dump['data']);
    }

    /**
     * Tests if the destructor nicely cleans up old files
     *
     * @return void
     */
    public function testDestructCleanup(): void
    {
        // Creates a file that's apparently created 1 day back.
        $testFile = static::$logPath . '/oldfile.debug';
        touch($testFile, strtotime('-1 DAY'));
        $this->assertFileExists($testFile);

        // Start and stops the service, which should trigger a cleanup
        $service = new DeveloperToolbar(new Request(['REQUEST_URI' => '/']), static::$logPath);
        $service->__destruct();

        $this->assertFileDoesNotExist($testFile);
    }
}
