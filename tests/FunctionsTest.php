<?php

use Miniframe\Core\Request;
use Miniframe\Toolbar\Service\DeveloperToolbar as DeveloperToolbarService;
use Miniframe\Toolbar\Middleware\DeveloperToolbarTest;
use Miniframe\Toolbar\AbstractDeveloperToolbarTest;

class FunctionsTest extends AbstractDeveloperToolbarTest
{
    /**
     * Tests a dump() without toolbar
     *
     * @return void
     */
    public function testCleanDump(): void
    {
        ob_start();
        dump(['foo', 'bar']);
        $data = ob_get_clean();
        $this->assertStringContainsString('foo', $data);
        $this->assertStringContainsString('bar', $data);
    }

    /**
     * Tests a dump() with toolbar
     *
     * @return void
     */
    public function testDebugDump(): void
    {
        $request = new Request(['REQUEST_URI' => '/']);
        $service = new DeveloperToolbarService($request, static::$logPath);
        ob_start();
        dump(['foo', 'bar', $line = __LINE__]);

        // Output buffer should be empty now
        $data = ob_get_clean();
        $this->assertEquals('', $data);

        // Is the dump in the log?
        $service->__destruct(); // Force a save action
        $log = $service->getDataByHash($service->getRequestHash());
        $this->assertIsArray($log);
        $this->assertArrayHasKey('dumps', $log);
        $this->assertIsArray($log['dumps']);
        $this->assertCount(1, $log['dumps']);

        // Is the dump valid?
        $dump = $log['dumps'][0];
        $this->assertArrayHasKey('file', $dump);
        $this->assertEquals(__FILE__, $dump['file']);
        $this->assertArrayHasKey('line', $dump);
        $this->assertEquals($line, $dump['line']);
        $this->assertArrayHasKey('data', $dump);
        $this->assertEquals(['foo', 'bar', $line], $dump['data']);
    }
}
