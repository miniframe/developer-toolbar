<?php

namespace Miniframe\Toolbar\Controller;

use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;
use Miniframe\Toolbar\AbstractDeveloperToolbarTest;

class DeveloperToolbarTest extends AbstractDeveloperToolbarTest
{
    /**
     * Tests the bug icon asset
     *
     * @return void
     */
    public function testBugicon(): void
    {
        // Test controller
        $controller = new DeveloperToolbar(new Request(['REQUEST_URI' => '/_DEBUG/bugicon.svg']), static::$config);
        $response = $controller->bugicon();

        // Assert result
        $this->assertArrayHasHeader('Content-type', 'image/svg+xml', $response->getHeaders());
        $this->assertStringContainsString('<svg', $response->render());
    }

    /**
     * Dataprovider for different tests that require a simulated debugging result
     *
     * @return array[]
     */
    public function dataProvider(): array
    {
        $baseRequest = new Request([
            'REQUEST_METHOD'  => 'GET',
            'REQUEST_URI'     => '/',
            'SERVER_PROTOCOL' => 'HTTP/1.1',
        ]);
        $baseData = array(
            'request' => $baseRequest,
            'requestHeaders' => array(
                'Content-type: text/html; charset=UTF-8',
            ),
            'rusage' => array(
                'start' => [
                    'ru_utime.tv_sec' => 0,
                    'ru_utime.tv_usec' => 1337,
                    'ru_stime.tv_sec' => 0,
                    'ru_stime.tv_usec' => 2448,
                ],
                'end' => [
                    'ru_utime.tv_sec' => 1,
                    'ru_utime.tv_usec' => 1337,
                    'ru_stime.tv_sec' => 2,
                    'ru_stime.tv_usec' => 2448,
                ],
            ),
            'responseHeaders' => array(
                'X-Debug-URL: /_DEBUG/notweedbuthash',
            ),
            'responseType' => Response::class,
            'responseCode' => 200,
            'exitCode' => 0,
            'errors' => array(),
            'memoryPeak' => array('real' => 2097152, 'emalloc' => 1337012),
            'dumps' => array(
                [
                    'file' => __FILE__,
                    'line' => __LINE__,
                    'data' => ['foo' => 'bar'],
                ]
            ),
            'exceptionThrown' => false,
            'exceptionType' => '',
            'exceptionCode' => 0,
            'exceptionMessage' => '',
            'exceptionFile' => __FILE__,
            'exceptionLine' => __LINE__,
            'exceptionTrace' => '',
        );

        // Start with the basic data set
        $return = array(
            [
                'requestData'       => $baseData,
                'userTimeMs'        => (float)1,
                'systemTimeMs'      => (float)2,
                'memoryPeakReal'    => '2.00MB',
                'memoryPeakEmalloc' => '1.28MB',
            ],
        );

        // Lower memory values
        $baseData['memoryPeak']['real']    = 2048;
        $baseData['memoryPeak']['emalloc'] = 128;
        $return[] = [
            'requestData'       => $baseData,
            'userTimeMs'        => (float)1,
            'systemTimeMs'      => (float)2,
            'memoryPeakReal'    => '2.00kB',
            'memoryPeakEmalloc' => '128 bytes',
        ];

        // Higher memory values
        $baseData['memoryPeak']['real']    = 2147483648;
        $baseData['memoryPeak']['emalloc'] = 2000000000;
        $return[] = [
            'requestData'       => $baseData,
            'userTimeMs'        => (float)1,
            'systemTimeMs'      => (float)2,
            'memoryPeakReal'    => '2.00GB',
            'memoryPeakEmalloc' => '1.86GB',
        ];

        return $return;
    }

    /**
     * Tests the json() method
     *
     * @param array  $requestData       The debug data.
     * @param float  $userTimeMs        The calculated user time in µs.
     * @param float  $systemTimeMs      The calculated system time in µs.
     * @param string $memoryPeakReal    The real peak memory, formatted.
     * @param string $memoryPeakEmalloc The emalloc peak memory, formatted.
     *
     * @return       void
     * @dataProvider dataProvider
     */
    public function testJson(
        array $requestData,
        float $userTimeMs,
        float $systemTimeMs,
        string $memoryPeakReal,
        string $memoryPeakEmalloc
    ): void {
        // Run controller
        $response = (new DeveloperToolbar(
            new Request(['REQUEST_URI' => '/_DEBUG/hash/json']),
            static::$config,
            $requestData
        ))->json();

        // Assert basics
        $this->assertArrayHasHeader('content-type', 'application/json', $response->getHeaders());
        $this->assertJson($response->render());

        // Decode data
        $parsedData = json_decode($response->render(), true);

        // Validate all data; most data is just what goes in must come back.
        $this->assertEquals($requestData['request']->getServer('REQUEST_METHOD'), $parsedData['requestMethod']);
        $this->assertEquals($requestData['request']->getServer('SERVER_PROTOCOL'), $parsedData['requestVersion']);
        $this->assertEquals($requestData['request']->getServer('REQUEST_URI'), $parsedData['requestUri']);
        $this->assertEquals($requestData['requestHeaders'], $parsedData['requestHeaders']);

        $this->assertEquals($requestData['responseHeaders'], $parsedData['responseHeaders']);
        $this->assertEquals($requestData['responseType'], $parsedData['responseType']);
        $this->assertEquals($requestData['responseCode'], $parsedData['responseCode']);
        $this->assertEquals($requestData['exitCode'], $parsedData['exitCode']);

        $this->assertEquals($requestData['errors'], $parsedData['errors']);
        $this->assertEquals(count($requestData['dumps']), count($parsedData['dumps']));

        // Loading time and memory is formatted;
        $this->assertEquals($userTimeMs, $parsedData['userTimeMs']);
        $this->assertEquals($systemTimeMs, $parsedData['systemTimeMs']);
        $this->assertEquals($memoryPeakReal, $parsedData['memoryPeakReal']);
        $this->assertEquals($memoryPeakEmalloc, $parsedData['memoryPeakEmalloc']);
    }

    /**
     * Tests the view method
     *
     * @param array $requestData The debug data.
     *
     * @return       void
     * @dataProvider dataProvider
     */
    public function testView(array $requestData): void
    {
        // Run controller
        $response = (new DeveloperToolbar(
            new Request(['REQUEST_URI' => '/_DEBUG/hash/json']),
            static::$config,
            $requestData
        ))->view();

        $this->assertInstanceOf(PhpResponse::class, $response);
    }
}
